/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class GreenAdapter extends RecyclerView.Adapter<GreenAdapter.NumberViewHolder> {

    // TODO (1) Crie um recurso de layout em 'res/layout/' chamado number_list_item.xml

    // Faça as etapas 2 - 11 dentro de number_list_item.xml
    // TODO (2) Configure o root layout para FrameLayout
    // TODO (3) Configure a largura para match_parent e a altura para wrap_content
    // TODO (4) Configure o padding para 16dp
    // TODO (5) Adicione um TextView como filho do FrameLayout
    // TODO (6) Configure o id do TextView par '@+id/tv_item_number'
    // TODO (7) Configure a largura e altura para wrap_content
    // TODO (8) Alinhe o TextView para o início (start) do pai
    // TODO (9) Centralize verticalemnte o TextView no layout
    // TODO (10) Configure a fonte para monospace
    // TODO (11) Configure o tamanho da fonte para 42sp

    private static final String TAG = GreenAdapter.class.getSimpleName();

    private int mNumberItems;

    /**
     * Constructor for GreenAdapter that accepts a number of items to display and the specification
     * for the ListItemClickListener.
     *
     * @param numberOfItems Number of items to display in list
     */
    public GreenAdapter(int numberOfItems) {
        mNumberItems = numberOfItems;
    }

    /**
     *
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param viewGroup The ViewGroup that these ViewHolders are contained within.
     * @param viewType  If your RecyclerView has more than one type of item (which ours doesn't) you
     *                  can use this viewType integer to provide a different layout. See
     *                  {@link android.support.v7.widget.RecyclerView.Adapter#getItemViewType(int)}
     *                  for more details.
     * @return A new NumberViewHolder that holds the View for each list item
     */
    @Override
    public NumberViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.number_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        NumberViewHolder viewHolder = new NumberViewHolder(view);

        return viewHolder;
    }

    /**
     * OnBindViewHolder is called by the RecyclerView to display the data at the specified
     * position. In this method, we update the contents of the ViewHolder to display the correct
     * indices in the list for this particular position, using the "position" argument that is conveniently
     * passed into us.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(NumberViewHolder holder, int position) {
        Log.d(TAG, "#" + position);
        holder.bind(position);
    }

    /**
     * This method simply returns the number of items to display. It is used behind the scenes
     * to help layout our Views and for animations.
     *
     * @return The number of items available in our forecast
     */
    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    // TODO (12) Crie uma classe chamada NumberViewHolder que estende de RecyclerView.ViewHolder

    // TODO (13) Dentro da NumberViewHolder, crie uma variável do tipo TextView chamada listItemNumberView

    // TODO (14) Crie um construtor que aceita um View chamado itemView como parâmetro
    // TODO (15) Dentro do construtor, chame super(itemView) e configure o id da listItemNumberView

    // TODO (16) Dentro da NumberViewHolder, crie um método void chamado 'bind' que aceita como parâmetro um int chamado listIndex
    // TODO (17) Dentro de bind, configure o texto de listItemNumberView para listIndex
    // TODO (18) Tenha cuidado para converter int (listIndex) para string

    }
}
