package com.example.android.recyclerview;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by cicerolp on 9/12/17.
 */

public class Friend {
    private int id;
    private int img;
    private String name;
    private String status;
    ArrayList<String> messasges;

    public Friend(int id, int img, String name, String status) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getMessasges() {
        return messasges;
    }

    public void setMessasges(ArrayList<String> messasges) {
        this.messasges = messasges;
    }
}
