/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.explicitintent;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText mNameEntry;
    private Button mDoSomethingCoolButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDoSomethingCoolButton = (Button) findViewById(R.id.b_do_something_cool);
        mNameEntry = (EditText) findViewById(R.id.et_text_entry);

        mDoSomethingCoolButton.setOnClickListener(new OnClickListener() {

            /**
             * The onClick method is triggered when this button (mDoSomethingCoolButton) is clicked.
             *
             * @param v The view that is clicked. In this case, it's mDoSomethingCoolButton.
             */
            @Override
            public void onClick(View v) {
                Context context = MainActivity.this;
                String message = "Botão clicado!";
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }
}

// TODO (1) Utilize o Android Studio para criar uma Atividade Em Branco chamada ChildActivity

// Faça os passos 2 - 5 no arquivo activity_child.xml
// TODO (2) Mude de ConstraintLayout para FrameLayout e faça os ajustes necessários
// TODO (3) Adicione um TextView e configure o ID para tv_display
// TODO (4) Configure o texto padrão
// TODO (5) Configure o tamanho da fonte

// Faça os passos 6 e 7 no arquivo ChildActivity.java
// TODO (6) Crie uma variável do tipo TextView para exibir uma mensagem
// TODO (7) Utilize findViewById para obter a referência do arquivo XML