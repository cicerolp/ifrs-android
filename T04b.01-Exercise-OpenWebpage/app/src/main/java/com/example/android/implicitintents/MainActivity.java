/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.implicitintents;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickOpenWebpageButton(View v) {
        // TODO (5) Crie uma string que contenha uma URL
        // Tenha certeza que o endereço comece com http:// ou https://

        // TODO (6) Substitua o código abaixo com a chamada para abrir uma página - openWebPage
        Toast.makeText(this, "TODO: Chame o método para abrir uma página", Toast.LENGTH_SHORT).show();
    }

    public void onClickOpenAddressButton(View v) {
        /*
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("geo")
                .path("0,0")
                .query(addressString);
        Uri addressUri = builder.build();
        */

        Toast.makeText(this, "TODO: Abra um mapa", Toast.LENGTH_SHORT).show();
    }

    public void onClickShareTextButton(View v) {
        Toast.makeText(this, "TODO: Compartilhe um texto", Toast.LENGTH_LONG).show();
    }

    public void createYourOwn(View v) {
        // veja em http://developer.android.com/guide/components/intents-common.html
        Toast.makeText(this, "TODO: Crie sua Intent", Toast.LENGTH_SHORT).show();
    }

    public void showMap(Uri geoLocation) {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setData(geoLocation);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void shareText(String textToShare) {
        String mimeType = "text/plain";

        String title = "Título da Janela";

        ShareCompat.IntentBuilder
                .from(this)
                .setType(mimeType)
                .setChooserTitle(title)
                .setText(textToShare)
                .startChooser();
    }

    // TODO (1) Crie um método chamado openWebPage que aceita uma String como parâmetro
    // Faça as etapas 2 - 4 dentro de openWebPage

    // TODO (2) Utilize Uri.parse para converter a String para Uri
    // ... Uri webpage = Uri.parse(url);

    // TODO (3) Crie um Intent com os parâmetros Intent.ACTION_VIEW e o Uri
    // ... new Intent(Intent.ACTION_VIEW, webpage);

    // TODO (4) Verifique se a Intent pode ser iniciada e após chame startActivity
    // ... intent.resolveActivity(getPackageManager()) != null
}